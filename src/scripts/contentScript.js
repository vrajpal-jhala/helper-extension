import { days, relativeDays, months, pageLoadWait, postScrapeLimit, status, routes, crScrapeLimit, inboxScrapeLimit } from '../constants';

const wait = (exec, args, time = 2000) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(exec(args));
    }, time);
  });
}

const convertProfileURL = (url, origin = window.location.origin) => {
  return url.startsWith(origin) ? url : origin + url;
}

const convertToISODate = (dateTime) => {
  const currentDate = new Date();
  dateTime = dateTime.split('_');
  let date = dateTime[0].split(' ');

  if (months.includes(date[0])) {
    date = `${currentDate.getFullYear()}-${(months.findIndex(month => month === date[0]) + 1).toString().padStart(2, 0)}-${(date[1]).toString().padStart(2, 0)}`;
  } else {
    let relativeDay = null;

    if (relativeDays.includes(date[0])) {
      const diffDays = relativeDays.indexOf(date[0]);
      const diffDate = new Date();
      diffDate.setDate((currentDate.getDate() - diffDays));
      relativeDay = diffDate.getDay();
    }

    if (days.includes(date[0])) {
      relativeDay = days.findIndex(day => day === date[0]);
    }

    if (relativeDay === null) {
      console.log('Couldn\'t get date, moving ahead with current date!');
      relativeDay = currentDate.getDay();
    }

    const currentDay = currentDate.getDay();
    const diffDays = ((currentDay - relativeDay + 6) % 6) + 1;
    const diffDate = new Date();
    diffDate.setDate((currentDate.getDate() - diffDays));
    date = `${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${diffDate.getDate()}`;
  }

  try {
    return new Date(`${date} ${dateTime[1]}`).toISOString();
  } catch (err) {
    try {
      console.log(err);
      return new Date(`${date} 12:00 AM`).toISOString();
    } catch (err) {
      console.log(err);
      return new Date().toISOString();
    }
  }
}

let scrappedInbox = [];

const scrapInbox = async (profileURL, index = 0) => {
  const messageListElement = document.getElementsByClassName('msg-conversations-container__conversations-list')[0];
  const messageListItem = messageListElement && messageListElement.children[index];

  const hasPill = messageListItem.getElementsByClassName('msg-conversation-card__pill').length > 0;
  const premiumInboxLink = messageListItem.className.includes('msg-premium-mailboxes__mailbox');

  if ((hasPill || premiumInboxLink) && index < inboxScrapeLimit) {
    scrapInbox(profileURL, index + 1);
    return;
  }

  if (messageListItem && index < inboxScrapeLimit) {
    await wait(() => {
      messageListElement.scroll({
        top: index * 92,
        behavior: 'smooth'
      });
    });

    // send updates
    chrome.runtime.sendMessage({
      type: 'UPDATE_STATUS',
      route: routes.inboxScrapper,
    });

    chrome.storage.local.set({
      [routes.inboxScrapper]: {
        status: status.SCRAPPING,
        data: `${index + 1} / ${inboxScrapeLimit}`,
        updatedAt: new Date().toISOString(),
      },
    });

    messageListItem.getElementsByTagName('a')[0].dispatchEvent(new Event('click', { bubbles: true }));

    await wait(() => {
      const chatHeadingElement = document.getElementsByClassName('msg-title-bar')[0];
      const chatElement = document.getElementsByClassName('msg-s-message-list-content')[0];
      const messageElements = chatElement ? Array.from(chatElement.getElementsByClassName('msg-s-message-list__event')) : [];

      const chat = {
        name: chatHeadingElement.getElementsByTagName('h2')[0].innerHTML.replace(/\s\s+/g, ' ').trim(),
        userProfileLink: convertProfileURL(chatHeadingElement.getElementsByTagName('a')[0].href),
        messages: []
      };

      let chatDate = null;
      messageElements.forEach((message) => {
        const dateTimes = Array.from(message.getElementsByTagName('time'));
        let userProfileLink = message.getElementsByTagName('a')[0];
        const userName = message.getElementsByClassName('msg-s-message-group__name')[0];
        const messageContent = message.getElementsByTagName('p')[0];

        if (userName && userProfileLink && dateTimes) {
          if (dateTimes.length === 2) {
            chatDate = dateTimes[0];
          } else {
            dateTimes.unshift(chatDate);
          }

          userProfileLink = convertProfileURL(userProfileLink.href);
          const dateString = dateTimes.map((dateTime) => dateTime.innerHTML.replace(/\s\s+/g, ' ').trim()).join('_').trim();

          chat.messages.push({
            userName: userName.innerHTML.replace(/\s\s+/g, ' ').trim(),
            userProfileLink,
            sent: profileURL === userProfileLink,
            dateTime: dateString.replace('_', ', '),
            isoDate: convertToISODate(dateString),
            messages: [],
          });
        }

        if (messageContent) {
          chat.messages[chat.messages.length - 1].messages.push(messageContent.innerHTML.replace('<!---->', '').replace('&nbsp;', ' '));
        }
      });

      scrappedInbox.push(chat);
    }, null, 3000);

    chrome.storage.local.set({
      'scrappedInbox': JSON.stringify(scrappedInbox),
      }, () => scrapInbox(profileURL, index + 1)
    );
  } else {
    // send updates
    chrome.runtime.sendMessage({
      type: 'UPDATE_STATUS',
      route: routes.inboxScrapper,
    });

    chrome.storage.local.set({
      [routes.inboxScrapper]: {
        status: status.COMPLETED,
        updatedAt: new Date().toISOString(),
      },
    });
  }
}

const getCurrentUserName = () => document.getElementsByClassName('app-header-item-content--type-entity')[0].innerText;

const scrapSalesNav = async (profileURL, index = 0) => {
  const messageListElement = document.getElementsByClassName('conversation-list-item');
  const messageListItem = messageListElement ? messageListElement[index] : null;

  if (messageListItem && index < inboxScrapeLimit) {
    await wait(() => {
      messageListItem.parentElement.parentElement.scroll({
        top: index * 92,
        behavior: 'smooth'
      });
    });

    // send updates
    chrome.runtime.sendMessage({
      type: 'UPDATE_STATUS',
      route: routes.inboxScrapper,
    });

    chrome.storage.local.set({
      [routes.inboxScrapper]: {
        status: status.SCRAPPING,
        data: `${index + 1} / ${inboxScrapeLimit}`,
        updatedAt: new Date().toISOString(),
      },
    });

    messageListItem.getElementsByTagName('a')[0].dispatchEvent(new Event('click', { bubbles: true }));

    await wait(async () => {     
      const findProfileLink = async () => {
        if (document.getElementsByClassName('conversation-insights__section')[0]) {
          return;
        } else {
          await wait(findProfileLink, null, 1000);
        }
      };

      await wait(findProfileLink, null, 0);

      const chatHeadingElement = document.getElementsByClassName('conversation-insights__section')[0].getElementsByClassName('artdeco-entity-lockup')[0];
      const chatElement = document.getElementsByClassName('thread-container')[0];
      const messageElements = chatElement ? Array.from(chatElement.getElementsByTagName('li')) : [];

      const chat = {
        name: chatHeadingElement.getElementsByClassName('artdeco-entity-lockup__title')[0].children[0].innerText.replace(/\s\s+/g, ' ').trim(),
        userProfileLink: convertProfileURL(chatHeadingElement.parentElement.href),
        messages: []
      };

      // chatHeadingElement.parentElement.removeAttribute('target');
      // chatHeadingElement.parentElement.removeAttribute('rel');
      // chatHeadingElement.parentElement.dispatchEvent(new Event('click', { bubbles: true }));

      // await wait(() => {
      //   console.log(document.getElementsByTagName('code'));
      //   Array.from(document.getElementsByTagName('code')).map((e) => {
      //     console.log(e.innerText.includes('"flagshipProfileUrl"'));
      //     if (e.innerText.includes('"flagshipProfileUrl"')) {
      //       chat.userProfileLink = JSON.parse(e.innerText).flagshipProfileUrl;
      //     }
      //   });
  
      // }, null, pageLoadWait);
 
      // await wait(() => window.history.go(-1), null, 3000);

      let chatDate = null;
      messageElements.forEach((message) => {
        const dateTimes = Array.from(message.getElementsByTagName('time'));
        let userProfileLink = { href: 'link' } || message.getElementsByTagName('a')[0];
        let userName = message.getElementsByTagName('address')[0];
        const messageContent = message.getElementsByTagName('p')[0];

        if (userName && userProfileLink && dateTimes) {
          if (dateTimes.length === 2) {
            chatDate = dateTimes[0];
          } else {
            dateTimes.unshift(chatDate);
          }

          userProfileLink = convertProfileURL(userProfileLink.href);
          const dateString = dateTimes.map((dateTime) => dateTime.innerHTML.replace(/\s\s+/g, ' ').trim()).join('_').trim();

          userName = userName.innerText.replace(/\s\s+/g, ' ').trim();
          chat.messages.push({
            userName: userName === 'You' ? getCurrentUserName() : userName,
            userProfileLink: userName === 'You' ? profileURL : chat.userProfileLink,
            sent: userName === 'You',
            dateTime: dateString.replace('_', ', '),
            isoDate: convertToISODate(dateString),
            messages: [],
          });
        }

        if (messageContent) {
          chat.messages[chat.messages.length - 1].messages.push(messageContent.innerHTML.replace('<!---->', '').replace('&nbsp;', ' '));
        }
      });

      scrappedInbox.push(chat);
    }, null, 3000);

    chrome.storage.local.set({
      'scrappedInbox': JSON.stringify(scrappedInbox),
      }, () => scrapSalesNav(profileURL, index + 1)
    );
  } else {
    // send updates
    chrome.runtime.sendMessage({
      type: 'UPDATE_STATUS',
      route: routes.inboxScrapper,
    });

    chrome.storage.local.set({
      [routes.inboxScrapper]: {
        status: status.COMPLETED,
        updatedAt: new Date().toISOString(),
      },
    });
  }
}

let likedPosts = [];
let recentCRs = [];

const scrapFeed = async (index = 0) => {
  const feed = document.querySelectorAll('[data-id^="urn:li:activity:"]');
  const post = feed[index];

  if (post && index < postScrapeLimit) {
    await wait((post) => {
      const y = post.getBoundingClientRect().top + window.scrollY;
      window.scroll({
        top: index === 0 ? y - 100 : y - 60,
        behavior: 'smooth'
      });
    }, post);

    // send updates
    chrome.runtime.sendMessage({
      type: 'UPDATE_STATUS',
      route: routes.feedScrapper,
    });

    chrome.storage.local.set({
      [routes.feedScrapper]: {
        status: status.SCRAPPING,
        data: `${index + 1} / ${postScrapeLimit}`,
        updatedAt: new Date().toISOString(),
      },
    });

    const reaction = post.getElementsByClassName('reactions-react-button')[0];

    if (reaction && reaction.querySelector('[alt="LIKE"], [alt="PRAISE"], [alt="APPRECIATION"], [alt="EMPATHY"], [alt="INTEREST"], [alt="MAYBE"]')) {
      const feedSharedActor = post.getElementsByClassName('feed-shared-actor__name')[0];
      const feedSharedMention = post.getElementsByClassName('feed-shared-text-view__mention')[0];
      const userName = feedSharedActor ? feedSharedActor.querySelector('span').innerHTML.replace(/\s\s+/g, ' ').trim() : feedSharedMention ? feedSharedMention.innerText : '';
      const userProfileElement = post.getElementsByClassName('feed-shared-actor__container-link')[0];
      const userProfileLink = userProfileElement.href.split('?')[0];
      const postContentElement = post.getElementsByClassName('feed-shared-text')[0];
      const postContent = postContentElement ? postContentElement.innerText.replace(/\s\s+/g, ' ').trim() : 'No text content! (eg. media content, embeded post etc.)';

      let postUrl = null;
      const postMenu = post.getElementsByClassName('feed-shared-control-menu__trigger')[0];
      postMenu.dispatchEvent(new Event('click', { bubbles: true }));

      const menuItems = post.getElementsByClassName('feed-shared-control-menu__headline');
      const menuItem = await wait((menuItems) => {
        let postUrlOption = null;
        Array.from(menuItems).forEach((menuItem) => {
          if (!postUrlOption && menuItem.innerHTML.replace(/\s\s+/g, ' ').trim() === 'Copy link to post') {
            postUrlOption = menuItem;
          }
        });

        return postUrlOption;
      }, menuItems, 4000);

      if (menuItem) {
        menuItem.dispatchEvent(new Event('click', { bubbles: true }));
        const actionToast = document.getElementsByClassName('artdeco-toast-item__cta')[0];

        if (actionToast) {
          postUrl = actionToast.href;
        }
      } else {
        postMenu.dispatchEvent(new Event('click', { bubbles: true }));
      }

      await wait(() => userProfileElement.dispatchEvent(new Event('click', { bubbles: true })), null, 3000);

      const scrollToExperience = async (index) => {
        await wait((index) => {
          if (!window.location.href.includes('company/') && document.getElementsByClassName('profile-detail')[0]) {
            const profileDetailElements = Array.from(document.getElementsByClassName('profile-detail')[0].children);
            const y = profileDetailElements[index].getBoundingClientRect().top + window.scrollY;
            window.scroll({
              top: y - 60,
              behavior: 'smooth'
            });

            const heading = profileDetailElements[index].getElementsByTagName('h2')[0];
            const isExperienceSection = (heading && heading.innerHTML.replace(/\s\s+/g, ' ').trim() !== 'Experience') || !heading;
            if (index + 1 < profileDetailElements.length && isExperienceSection) {
              scrollToExperience(index + 1);
            }
          }
        }, index);
      }

      await wait(scrollToExperience, 0, pageLoadWait);

      const companyData = await wait(() => {
        const companyData = {};
        const profileCardCompany = document.getElementsByClassName('pv-top-card--experience-list')[0];

        if (profileCardCompany) {
          companyData.companyName = profileCardCompany.getElementsByTagName('a')[0].innerText;
          Array.from(document.getElementsByClassName('background-details')[0].getElementsByClassName('pv-profile-section-pager')).forEach((element) => {
            if(element.getElementsByTagName('h2')[0].innerHTML.replace(/\s\s+/g, ' ').trim() === 'Experience') {
              const mostRecentCompany = element.getElementsByTagName('a')[0];

              if (mostRecentCompany.href.includes('/company')) {
                companyData.companyPageLink = mostRecentCompany.href;
                mostRecentCompany.dispatchEvent(new Event('click', { bubbles: true }));
              }
            };
          });
        }

        return companyData;
      }, null, pageLoadWait);

      if (companyData.companyPageLink) {
        companyData.companyIndustry = await wait(() => {
          const companyIndustry = document.getElementsByClassName('org-top-card-summary-info-list__info-item')[0];
          return companyIndustry ? companyIndustry.innerText : null;
        }, null, pageLoadWait);
      }

      if (companyData.companyPageLink) {
        window.history.go(-2);
      } else {
        window.history.go(-1);
      }

      await wait(() => {}, null, pageLoadWait);

      likedPosts.push({ userName, postContent, userProfileLink, postUrl, ...companyData });
    }

    chrome.storage.local.set({
      'scrappedFeed': JSON.stringify(likedPosts),
    }, () => scrapFeed(index + 1));
  } else {
    // send updates
    chrome.runtime.sendMessage({
      type: 'UPDATE_STATUS',
      route: routes.feedScrapper,
    });

    chrome.storage.local.set({
      [routes.feedScrapper]: {
        status: status.COMPLETED,
        updatedAt: new Date().toISOString(),
      },
    });
  }
}

const scrapCR = async (index = 0) => {
  const recentCRList = document.querySelectorAll('ul > .mn-connection-card');
  const cr = recentCRList[index];

  if (cr && index < crScrapeLimit) {
    // send updates
    chrome.runtime.sendMessage({
      type: 'UPDATE_STATUS',
      route: routes.crScrapper,
    });

    chrome.storage.local.set({
      [routes.crScrapper]: {
        status: status.SCRAPPING,
        data: `${index + 1} / ${crScrapeLimit}`,
        updatedAt: new Date().toISOString(),
      },
    });

    await wait(() => {
      const y = cr.getBoundingClientRect().top + window.scrollY;
      window.scroll({
        top: y - 60,
        behavior: 'smooth'
      });
    }, null, 500);

    recentCRs.push({
      userName: cr.getElementsByClassName('mn-connection-card__name')[0].innerText,
      userProfileLink: cr.getElementsByTagName('a')[0].href,
      connectedString: cr.getElementsByTagName('time')[0].innerText,
    });

    chrome.storage.local.set({
      'scrappedCR': JSON.stringify(recentCRs),
    }, () => scrapCR(index + 1));
    
  } else {
    // send updates
    chrome.runtime.sendMessage({
      type: 'UPDATE_STATUS',
      route: routes.crScrapper,
    });

    chrome.storage.local.set({
      [routes.crScrapper]: {
        status: status.COMPLETED,
        updatedAt: new Date().toISOString(),
      },
    });
  }
};

chrome.runtime.onMessage.addListener(({ type, profileURL }, _sender, _sendResponse) => {
  if (type.includes('SCRAPE_INBOX')) {
    if (window.location.href.startsWith('https://www.linkedin.com/messaging')
      || window.location.href.startsWith('https://www.linkedin.com/sales/inbox')) {
      // send updates
      chrome.runtime.sendMessage({
        type: 'UPDATE_STATUS',
        route: routes.inboxScrapper,
      });

      chrome.storage.local.set({
        [routes.inboxScrapper]: {
          status: status.SCRAPPING,
          updatedAt: new Date().toISOString(),
        },
      });

      scrappedInbox = [];
      chrome.storage.local.set({
        'isSalesNav': JSON.stringify(type === 'SCRAPE_INBOX_SN'),
        }, () => {
          if (type === 'SCRAPE_INBOX_SN') {
            scrapSalesNav(profileURL);
          } else {
            scrapInbox(profileURL);
          }
        }
      );

      console.log('Scrapping started successfully!');
    } else {
      console.log('Please go to https://www.linkedin.com/messaging and try again!');
    }
  }

  if (type === 'PASTE_INBOX') {
    localStorage.removeItem('isSalesNav');
    chrome.storage.local.get(['scrappedInbox', 'isSalesNav'], ({ scrappedInbox, isSalesNav }) => {
      if (scrappedInbox) {
        console.log('Loaded Successfully!');
        localStorage.setItem('scrappedInbox', scrappedInbox);
        localStorage.setItem('isSalesNav', isSalesNav);
        document.getElementById('loadScrappedInbox').click();
      } else {
        console.log('Nothing to paste, please first scrape the LinkedIn inbox and try again!');
      }
    });
  }

  if (type === 'PASTE_COOKIE') {
    chrome.storage.local.get(['sessionCookie'], ({ sessionCookie }) => {
      if (sessionCookie) {
        console.log('Cookie pasted!');
        localStorage.setItem('liAtSessionCookie', sessionCookie);
        document.getElementById('loadScrappedInbox').click();
      } else {
        console.log('No cookie to paste!');
      }
    });
  }

  if (type === 'SCRAPE_FEED') {
    if (window.location.href.startsWith('https://www.linkedin.com/feed')) {
      // send updates
      chrome.runtime.sendMessage({
        type: 'UPDATE_STATUS',
        route: routes.feedScrapper,
      });

      chrome.storage.local.set({
        [routes.feedScrapper]: {
          status: status.SCRAPPING,
          updatedAt: new Date().toISOString(),
        },
      });

      likedPosts = [];
      scrapFeed();

      console.log('Scrapping started successfully!');
    } else {
      console.log('Please go to https://www.linkedin.com/feed and try again!');
    }
  }

  if (type === 'PASTE_FEED') {
    chrome.storage.local.get(['scrappedFeed'], ({ scrappedFeed }) => {
      if (scrappedFeed) {
        console.log('Loaded Successfully!');
        localStorage.setItem('scrappedFeed', scrappedFeed);
        document.getElementById('loadScrappedFeed').click();
      } else {
        console.log('Nothing to paste, please first scrape the LinkedIn feed and try again!');
      }
    });
  }

  if (type === 'SCRAPE_CR') {
    if (window.location.href.startsWith('https://www.linkedin.com/mynetwork/invite-connect/connections/')) {
      const sortBy = document.querySelector('[aria-label*="Sorting options for connections list"').innerText.trim();

      if (sortBy === 'Recently added') {
        // send updates
        chrome.runtime.sendMessage({
          type: 'UPDATE_STATUS',
          route: routes.crScrapper,
        });

        chrome.storage.local.set({
          [routes.crScrapper]: {
            status: status.SCRAPPING,
            updatedAt: new Date().toISOString(),
          },
        });

        recentCRs = [];
        scrapCR();
      } else {
        // send updates
        chrome.runtime.sendMessage({
          type: 'ERROR',
          errorMessage: 'Please sort the list by recently added and try again!',
        });        
      }

      console.log('Scrapping started successfully!');
    } else {
      console.log('Please go to https://www.linkedin.com/mynetwork/invite-connect/connections/ and try again!');
    }
  }

  if (type === 'PASTE_CR') {
    chrome.storage.local.get(['scrappedCR'], ({ scrappedCR }) => {
      if (scrappedCR) {
        console.log('Loaded Successfully!');
        localStorage.setItem('scrappedCR', scrappedCR);
        document.getElementById('loadScrappedCR').click();
      } else {
        console.log('Nothing to paste, please first scrape the LinkedIn recent connections and try again!');
      }
    });
  }
});
